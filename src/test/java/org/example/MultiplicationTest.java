package org.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MultiplicationTest {

    @Test
    public void TestMult(){
        Multiplication multiplication = new Multiplication();
        assertEquals(15, multiplication.mult(5,3));
    }
}
