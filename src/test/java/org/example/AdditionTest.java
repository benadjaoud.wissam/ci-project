package org.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AdditionTest {

    @Test
    public void TestAdd(){
        Addition add = new Addition();
        assertEquals(5, add.add(3,2));
    }
}
